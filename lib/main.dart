import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laundromat/src/app.dart';
import 'package:laundromat/src/blocs/authentication/authentication.dart';
import 'package:laundromat/src/data/repository/remote_user_repository.dart';

main() {
  runApp(
    BlocProvider<AuthenticationBloc>(
      create: (context) => AuthenticationBloc()..add(AppStarted()),
      child: MultiRepositoryProvider(
        providers: [
          RepositoryProvider<RemoteUserRepository>(
            create: (context) => RemoteUserRepository(),
          ),
        ],
        child: MyApp(),
      ),
    ),
    //MyApp()
    /*ChangeNotifierProvider<CartStateBloc>(
      create: (_) => CartStateBloc(),//..loadProducts()
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashScreenPage(),
      ),
    ),*/
  );
}

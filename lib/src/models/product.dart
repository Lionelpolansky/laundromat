import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class Category {
  final String id;
  final String name;
  final String image;
  final String description;

  Category({this.id, this.name, this.image, this.description});

  static Category fromJson(dynamic json) {
    return Category(
      id: json['uuid'],
      name: json['name'],
      image: json['image'],
      description: json['description'],
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        id,
        name,
        image,
        description,
      ];

  @override
  String toString() => '$name';
}

class Product {
  const Product({
    @required this.id,
    @required this.name,
    @required this.image,
    @required this.price,
    @required this.cleaningTime,
    @required this.category,
  })  : assert(category != null),
        assert(id != null),
        assert(cleaningTime != null),
        assert(name != null),
        assert(image != null),
        assert(price != null);

  final String id;
  final String name;
  final String image;
  final double price;
  final int cleaningTime;
  final Category category;

  static Product fromJson(dynamic json) {
    final categoryinfo = json['category'];
    return Product(
      id: json['uuid'],
      name: json['name'],
      image: json['image'],
      cleaningTime: json['unit_time'] as int,
      price: double.parse(json['unit_price']),
      category: Category(
        id: categoryinfo['uuid'],
        name: categoryinfo['name'],
        image: categoryinfo['image'],
        description: categoryinfo['description'],
      ),
    );
  }

  @override
  String toString() => '$name ( category = $category ) ';

  @override
  // TODO: implement props
  List<Object> get props => [
        id,
        name,
        image,
        cleaningTime,
        price,
      ];
}

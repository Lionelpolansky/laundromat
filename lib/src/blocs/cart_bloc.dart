import 'package:flutter/foundation.dart' as foundation;
import 'package:laundromat/src/data/repository/product_api_client.dart';
import 'package:http/http.dart' as http;
import '../models/product.dart';

class CartStateBloc extends foundation.ChangeNotifier {
  List<Product> _availableProducts;

  List<Category> _availableCategories;

  Category _selectedCategory = null;

  final _productsInCart = <String, int>{};


  Map<String, int> get productsInCart {
    return Map.from(_productsInCart);
  }

  int getQuantityByProductId(String productId){
    if(_productsInCart.containsKey(productId)){
      return _productsInCart[productId];
    }else{
      return null;
    }
  }

  int get totalCartQuantity {
    return _productsInCart.values.fold(0, (accumulator, value) {
      return accumulator + value;
    });
  }

  Category get selectedCategory {
    return _selectedCategory;
  }

  double get subtotalCost {
    return _productsInCart.keys.map((id) {
      return getProductById(id).price * _productsInCart[id];
    }).fold(0, (accumulator, extendedPrice) {
      return accumulator + extendedPrice;
    });
  }

  double get totalCost {
    return subtotalCost;
  }


  List<Product> getProducts() {
    if (_availableProducts == null) {
      return [];
    }

    if (_selectedCategory == null) {
      return List.from(_availableProducts);
    } else {
      return _availableProducts.where((p) {
        return p.category.id == _selectedCategory.id;
      }).toList();
    }
  }

  List<Product> getSearchProducts() {
    if (_availableProducts == null) {
      return [];
    }
     return List.from(_availableProducts);

  }

  List<Category> getCategories() {
    if (_availableCategories == null) {
      return [];
    }
    return List.from(_availableCategories);
  }

  List<Product> search(String searchTerms) {
    return getSearchProducts().where((product) {
      return product.name.toLowerCase().contains(searchTerms.toLowerCase());
    }).toList();
  }

  void addProductToCart(String productId) {
    if (!_productsInCart.containsKey(productId)) {
      _productsInCart[productId] = 1;
    } else {
      _productsInCart[productId]++;
    }

    notifyListeners();
  }

  void removeItemFromCart(String productId) {
    if (_productsInCart.containsKey(productId)) {
      if (_productsInCart[productId] == 1) {
        _productsInCart.remove(productId);
      } else {
        _productsInCart[productId]--;
      }
    }

    notifyListeners();
  }

  Product getProductById(String id) {
    return _availableProducts.firstWhere((p) => p.id == id);
  }

  void removeAllItemsFromCart(String productId) {
    _productsInCart.remove(productId);
    notifyListeners();
  }

  void clearCart() {
    _productsInCart.clear();
    notifyListeners();
  }

  Future<void> loadProducts() async {
    _availableProducts = await ProductApiClient(http.Client()).fetchProducts();
    _availableCategories = await ProductApiClient(http.Client()).fetchCategories();
    notifyListeners();
  }

  void setCategory(Category newCategory) {
    _selectedCategory = newCategory;
    //notifyListeners();
  }
}
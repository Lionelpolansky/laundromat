import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laundromat/src/blocs/authentication/authentication.dart';
import 'package:laundromat/src/data/repository/remote_user_repository.dart';
import 'sign_in.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final RemoteUserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  SignInBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null),
        super(SignInInitialState());

  @override
  Stream<SignInState> mapEventToState(
    SignInEvent event,
  ) async* {
    // normal sign in
    if (event is SignInPressed) {
      yield SignInProcessingState();
      try {
        var token = await userRepository.signIn(
          email: event.email,
          password: event.password,
        );
        authenticationBloc.add(LoggedIn(token));
        yield SignInFinishedState();
      } catch (error) {
        yield SignInErrorState(error);
      }
    }
  }
}

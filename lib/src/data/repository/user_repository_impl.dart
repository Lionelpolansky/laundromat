import 'package:flutter/material.dart';
import 'package:laundromat/src/data/repository/remote_user_repository.dart';
import 'package:laundromat/src/data/repository/user_repository.dart';

class UserRepositoryImpl extends UserRepository {
  final RemoteUserRepository remoteUserRepository;

  UserRepositoryImpl({@required this.remoteUserRepository});

  @override
  Future<String> signIn({
    @required String email,
    @required String password,
  }) async {
    return remoteUserRepository.signIn(email: email, password: password);
  }



}

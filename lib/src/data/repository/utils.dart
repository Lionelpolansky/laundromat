
class HttpClient {
  Uri createUri(String route, [Map<String, String> param = const {}]) {
    return Uri(
      scheme: 'https',
      host: "https://api.doover.tech/",
      path: route,
      queryParameters: param,
    );
  }
}

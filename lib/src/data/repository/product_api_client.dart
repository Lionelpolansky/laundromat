import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:laundromat/src/models/product.dart';

class ProductApiClient {
  static const baseURL = 'https://api.doover.tech/';
  final http.Client httpClient;

  ProductApiClient(this.httpClient);

  Future<List<Product>> fetchProducts() async {
    final response = await this.httpClient.get('${baseURL}catalog/');
    if (response.statusCode == 200) {
      final Json = jsonDecode(utf8.decode(response.bodyBytes)) as List;
      return Json.map((rawPost) {
        return Product.fromJson(rawPost);
      }).toList();
    } else {
      throw Exception('error fetching posts');
    }
  }
  Future<List<Category>> fetchCategories() async {
    final response = await this.httpClient.get('${baseURL}catalog/categories/');
    if (response.statusCode == 200) {
      final Json = jsonDecode(utf8.decode(response.bodyBytes)) as List;
      return Json.map((rawPost) {
        return Category.fromJson(rawPost);
      }).toList();
    } else {
      throw Exception('error fetching posts');
    }
  }
}

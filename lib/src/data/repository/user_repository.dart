import 'package:flutter/material.dart';

abstract class UserRepository {
  Future<String> signIn({
    @required String email,
    @required String password,
  });

}

import 'package:flutter/cupertino.dart';
import 'package:laundromat/src/data/repository/product_api_client.dart';
import 'package:laundromat/src/models/product.dart';

class ProductrRepository{
  final ProductApiClient productApiClient;

  ProductrRepository({@required this.productApiClient}): assert(productApiClient != null);

  Future<List<Product>> getProducts() async {
    List<Product> allProducts = await productApiClient.fetchProducts();
    return allProducts;
  }


}
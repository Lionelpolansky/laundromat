import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:laundromat/src/data/repository/user_repository.dart';
import 'package:laundromat/src/data/repository/utils.dart';

class RemoteUserRepository extends UserRepository {
  @override
  Future<String> signIn({
    @required String email,
    @required String password,
  }) async {
    var route = HttpClient().createUri("auth/login/");
    var data = <String, String>{
      'username': email,
      'password': password,
    };

    var response = await http.post(route, body: data);
    Map jsonResponse = json.decode(response.body);
    if (response.statusCode != 200) {
      throw jsonResponse['message'];
    }
    return jsonResponse['token'];
  }

}

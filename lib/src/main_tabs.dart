import 'package:flutter/material.dart';
import 'package:laundromat/src/ui/cart.dart';
import 'package:laundromat/src/ui/profile.dart';
import 'package:laundromat/src/ui/search.dart';
import 'package:laundromat/src/utils/UIData.dart';
import 'blocs/navigation/navigation_bloc.dart';

class MainTabs extends StatefulWidget {
  createState() => _MainTabsState();
}

class _MainTabsState extends State<MainTabs> {
  BottomNavBarBloc _bottomNavBarBloc;

  @override
  void initState() {
    super.initState();
    _bottomNavBarBloc = BottomNavBarBloc();
  }

  @override
  void dispose() {
    _bottomNavBarBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<NavBarItem>(
        stream: _bottomNavBarBloc.itemStream,
        initialData: _bottomNavBarBloc.defaultItem,
        builder: (BuildContext context, AsyncSnapshot<NavBarItem> snapshot) {
          switch (snapshot.data) {
            case NavBarItem.HOME:
              return SearchTab();
            case NavBarItem.PROFILE:
              return Profile();
            case NavBarItem.CART:
              return Cart();
          }
          return Container();
        },
      ),
      bottomNavigationBar: StreamBuilder(
        stream: _bottomNavBarBloc.itemStream,
        initialData: _bottomNavBarBloc.defaultItem,
        builder: (BuildContext context, AsyncSnapshot<NavBarItem> snapshot) {
          return BottomNavigationBar(
            //fixedColor: Colors.blueAccent,
            currentIndex: snapshot.data.index,
            onTap: _bottomNavBarBloc.pickItem,
            selectedItemColor: UIData.text_blue,
            //unselectedItemColor: UIData.textfield_gray,
            items: [
              BottomNavigationBarItem(
                  title: Text(
                    'Прачечная',
                    style: TextStyle(
                      //color: UIData.text_blue,
                      fontSize: 10.0,
                      fontFamily: "MuseoSansBold",
                    ),
                  ),
                  icon: Image.asset("assets/icons/Vector.png"),
                  activeIcon: Image.asset(
                    "assets/icons/Vector.png",
                    color: UIData.text_blue,
                  )),
              BottomNavigationBarItem(
                title: Text(
                  'Профиль',
                  style: TextStyle(
                    //color: UIData.text_blue,
                    fontSize: 10.0,
                    fontFamily: "MuseoSansBold",
                  ),
                ),
                icon: Image.asset("assets/icons/profile.png"),
                activeIcon: Image.asset(
                  "assets/icons/profile.png",
                  color: UIData.text_blue,
                ),
              ),
              BottomNavigationBarItem(
                title: Text(
                  'Корзина',
                  style: TextStyle(
                    //color: UIData.text_blue,
                    fontSize: 10.0,
                    fontFamily: "MuseoSansBold",
                  ),
                ),
                icon: Image.asset("assets/icons/cart.png"),
                activeIcon: Image.asset(
                  "assets/icons/cart.png",
                  color: UIData.text_blue,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laundromat/src/splash_screen.dart';
import 'package:provider/provider.dart';
import 'blocs/authentication/authentication.dart';
import 'blocs/cart_bloc.dart';
import 'blocs/sign_in/sign_in.dart';
import 'data/repository/remote_user_repository.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CartStateBloc()),
        Provider(create: (_) => AuthenticationBloc()),
        Provider(
          create: (_) => SignInBloc(
            userRepository: RepositoryProvider.of<RemoteUserRepository>(context),
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashScreenPage(),
      ),
    );
  }
}
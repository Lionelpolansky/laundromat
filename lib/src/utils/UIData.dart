import 'dart:ui';

class UIData{
  //colors
  static const Color background_gray = Color(0xffE5E5E5);
  static const Color text_blue = Color(0xff172853);
  static const Color text_red = Color(0xffFF5050);
  static const Color divider_gray = Color(0xffEDF2FF);
  static const Color button_blue = Color(0xff4376FB);
  static const Color textfield_gray = Color(0xffB0B3BC);


}
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundromat/src/main_tabs.dart';
import 'package:laundromat/src/blocs/cart_bloc.dart';
import 'package:laundromat/src/ui/sign_in.dart';
import 'package:provider/provider.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future loadData() async {
    final bloc = Provider.of<CartStateBloc>(context,listen: false);
    await bloc.loadProducts();
    //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => App()));
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignInPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: CupertinoActivityIndicator(),
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundromat/src/blocs/cart_bloc.dart';
import 'package:laundromat/src/models/product.dart';
import 'package:laundromat/src/ui/widgets/product_item.dart';
import 'package:laundromat/src/utils/UIData.dart';
import 'package:provider/provider.dart';

class LaundryCategoryListPage extends StatelessWidget {
  final Category category;

  const LaundryCategoryListPage({Key key, this.category}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Consumer<CartStateBloc>(
      builder: (context, bloc, child) {
        //bloc.setCategory(category);
        final products = bloc.getProducts();
        return Scaffold(
          backgroundColor: UIData.background_gray,
          body: CustomScrollView(
            semanticChildCount: products.length,
            slivers: [
              SliverAppBar(
                title:
                    Text(category.toString(), style: TextStyle(fontSize: 17.0, color: UIData.text_blue, fontFamily: "MuseoSansBold")),
                centerTitle: true,
                backgroundColor: Colors.white,
                elevation: 0.0,
                leading: IconButton(
                  color: UIData.text_blue,
                  padding: EdgeInsets.all(0.0),
                  onPressed: () {
                    Navigator.of(context).maybePop();
                  },
                  icon: Icon(Icons.arrow_back_ios),
                ),
              ),
              SliverSafeArea(
                top: false,
                minimum: const EdgeInsets.only(top: 16),
                sliver: SliverList(
                  delegate: SliverChildBuilderDelegate((context, index) {
                    if (index < products.length) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 8, left: 16, right: 16),
                        child: ProductItem(
                          product: products[index],
                        ),
                      );
                    }
                    return null;
                  }),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

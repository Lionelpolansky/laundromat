import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundromat/src/blocs/cart_bloc.dart';
import 'package:laundromat/src/ui/widgets/product_item.dart';
import 'package:laundromat/src/utils/UIData.dart';
import 'package:provider/provider.dart';

class Cart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String items = "Вещь";

    return Consumer<CartStateBloc>(
      builder: (context, bloc, child) {
        final products = bloc.productsInCart;
        var n = bloc.totalCartQuantity;
        if(n == 1){
          items = "Вещь";
        }else if(n < 5){
          items = "Вещи";
        }else{
          items = "Вещей";
        }
        return Scaffold(
          backgroundColor: UIData.background_gray,
          bottomNavigationBar: products.length > 0
              ? Material(
                  elevation: 26,
                  child: Container(
                    color: Colors.white,
                    //height: 100,
                    //elevation: 16,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Общая сумма ${bloc.totalCost.toInt()} тг",//${bloc.totalCost.toInt()}
                                style: TextStyle(color: UIData.text_blue, fontSize: 16.0, fontFamily: "MuseoSansBold"),
                              ),
                              Text(
                                "${bloc.totalCartQuantity} ${items}",
                                style: TextStyle(color: UIData.text_blue, fontSize: 14.0, fontFamily: "MuseoSansReg"),
                              ),
                            ],
                          ),
                          SizedBox(height: 24),
                          Container(
                            width: double.infinity,
                            //color: Colors.amber,
                            child: CupertinoButton(
                              color: UIData.button_blue,
                              child: Text(
                                "Оформить",
                                style: TextStyle(fontSize: 16.0, fontFamily: "MuseoSansBold"),
                              ),
                              onPressed: () {
                                _settingModalBottomSheet(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Container(
                  height: 100,
                  child: Center(
                    child: Text(
                      "Добавьте вещи на стирку",
                      style: TextStyle(color: UIData.text_blue, fontSize: 16.0, fontFamily: "MuseoSansBold"),
                    ),
                  ),
                ),
          body: CustomScrollView(
            //semanticChildCount: products.length,
            slivers: [
              SliverAppBar(
                title: Text("Корзина",
                    style: TextStyle(fontSize: 17.0, color: UIData.text_blue, fontFamily: "MuseoSansBold")),
                centerTitle: true,
                backgroundColor: Colors.white,
                elevation: 0.0,
                actions: [
                  CupertinoButton(
                    child: Text("Очистить",
                        style: TextStyle(fontSize: 17.0, color: UIData.text_red, fontFamily: "MuseoSansReg")),
                    onPressed: () {
                      bloc.clearCart();
                    },
                  ),
                ],
              ),
              SliverSafeArea(
                top: false,
                minimum: const EdgeInsets.only(top: 16),
                sliver: SliverList(
                  delegate: SliverChildBuilderDelegate((context, index) {
                    if (products.length > index) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 8, left: 16, right: 16),
                        child: ProductItem(
                          product: bloc.getProductById(bloc.productsInCart.keys.toList()[index]),
                          cart: true,
                        ),
                      );
                    }
                    return null;
                  }),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _settingModalBottomSheet(context){
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc){
          return Stack(
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 32,bottom: 64,left: 16,right: 16),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Спасибо",
                        style: TextStyle(color: UIData.text_blue, fontSize: 20.0, fontFamily: "MuseoSansBold"),
                      ),
                      SizedBox(height: 24),
                      Text(
                        "Ваш заказ принят, курьер заберет ваши вещи, через 2 рабочих дня вам доставят их обратно",
                        maxLines: 5,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: UIData.text_blue, fontSize: 14.0, fontFamily: "MuseoSansReg"),
                      ),
                    ],
                  ),
                ),
              ),

              Positioned(
                top: 8,
                right: 8,
                child: IconButton(
                  icon: Image.asset("assets/icons/close.png"),
                  onPressed: () {
                    Navigator.of(context).maybePop();
                  },
                  //padding: EdgeInsets.all(0.0),
                ),
              ),

            ],
          );
        }
    );
  }


}

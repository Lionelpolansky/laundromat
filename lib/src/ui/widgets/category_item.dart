import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundromat/src/blocs/cart_bloc.dart';
import 'package:laundromat/src/models/product.dart';
import 'package:laundromat/src/ui/laundry_category_list_page.dart';
import 'package:laundromat/src/utils/UIData.dart';
import 'package:provider/provider.dart';

class CategoryItem extends StatelessWidget {
  final Category category;

  const CategoryItem({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<CartStateBloc>(context, listen: false);
    return GestureDetector(
      onTap: () {
        bloc.setCategory(category);
        Navigator.push(
          context,
          CupertinoPageRoute(builder: (context) => LaundryCategoryListPage(category: category)),
        );
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          color: Colors.white,
          height: 90,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        category != null ? category.toString().split('.').last : "Офисная одежда",
                        style: TextStyle(
                            fontSize: 14.0,
                            color: UIData.text_blue,
                            fontFamily: "MuseoSansReg",
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: 10),
                      Text(
                        category.description,
                        //Верхняя одежда Джемперы Обувь
                        maxLines: 2,
                        //softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 12.0, color: UIData.textfield_gray, fontFamily: "MuseoSansReg"),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 30),
              Container(
                //width: 104,
                child: Image.asset("assets/images/shoe.png"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

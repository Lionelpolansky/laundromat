import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundromat/src/ui/sign_in.dart';
import 'package:laundromat/src/utils/UIData.dart';

class SettingsSwitcher extends StatefulWidget {
  final String str;
  final bool divider;

  const SettingsSwitcher({
    Key key,
    this.str = "switcher",
    this.divider = true,
  }) : super(key: key);

  @override
  _Settings_switcherState createState() => _Settings_switcherState();
}

class _Settings_switcherState extends State<SettingsSwitcher> {
  bool _lights = true;

  @override
  Widget build(BuildContext context) {
    _divider_line() {
      if (widget.divider) {
        return Divider(
          color: UIData.textfield_gray,
          thickness: 1.0,
          endIndent: 0.0,
          indent: 0.0,
          height: 1.0,
        );
      }
      return Container();
    }

    return Container(
      color: Colors.white,
      height: 119,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(widget.str,
                        style: TextStyle(fontSize: 16.0, fontFamily: "MuseoSansReg", color: UIData.text_blue)),
                    CupertinoSwitch(
                      value: _lights,
                      activeColor: Color(0xff4376FB),
                      onChanged: (bool value) {
                        setState(() {
                          _lights = value;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
            _divider_line(),
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => SignInPage()), (Route<dynamic> route) => false);
                },
                child: Container(
                  color: Colors.white,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Выйти",
                          style: TextStyle(fontSize: 16.0, fontFamily: "MuseoSansReg", color: UIData.text_red)),
                      SizedBox(
                        width: 1.0,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

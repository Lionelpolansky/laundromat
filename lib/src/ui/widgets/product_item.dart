import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundromat/src/blocs/cart_bloc.dart';
import 'package:laundromat/src/models/product.dart';
import 'package:laundromat/src/utils/UIData.dart';
import 'package:provider/provider.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  final bool cart;

  const ProductItem({
    Key key,
    this.product,
    this.cart = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<CartStateBloc>(context, listen: false);
    var quantity = bloc.getQuantityByProductId(product.id);
    int n = product.cleaningTime;
    String days = "день";
    if(n == 1){
      days = "день";
    }else if(n < 5){
      days = "дня";
    }else{
      days = "дней";
    }
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: Container(
            color: Colors.white,
            height: 102,
            child: Row(
              children: [
                Container(
                  //color: Colors.purple,
                  width: 60,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 20.0),
                    child: Image.asset("assets/images/shirt.png"),
                  ),
                ),
                Expanded(
                  child: Container(
                    //color: Colors.amber,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(product.name,
                                  style:
                                      TextStyle(fontSize: 17.0, color: UIData.text_blue, fontFamily: "MuseoSansReg")),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Text("Срок чистки / ",
                                      style: TextStyle(fontSize: 14.0, color: Colors.grey, fontFamily: "MuseoSansReg")),
                                  Text("${product.cleaningTime} ${days}",
                                      style: TextStyle(
                                          fontSize: 14.0, color: UIData.text_blue, fontFamily: "MuseoSansReg")),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(child: Container()),
                        Container(
                          //color: Colors.blueAccent,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              IconButton(
                                icon: Image.asset("assets/icons/add_button.png"),
                                onPressed: () {
                                  bloc.addProductToCart(product.id);
                                },
                                padding: EdgeInsets.all(0.0),
                              ),
                              quantity != null
                                  ? Text(quantity.toString(),
                                      style: TextStyle(
                                          fontSize: 14.0, color: UIData.text_blue, fontFamily: "MuseoSansReg"))
                                  : Container(),
                              quantity != null
                                  ? IconButton(
                                      padding: EdgeInsets.all(0.0),
                                      onPressed: () {
                                        bloc.removeItemFromCart(product.id);
                                      },
                                      icon: Image.asset("assets/icons/delete_button.png"),
                                    )
                                  : Container(),
                              Expanded(child: Container()),
                              Text("${product.price.toInt()} тг",
                                  style:
                                      TextStyle(fontSize: 14.0, color: UIData.text_blue, fontFamily: "MuseoSansReg")),
                              SizedBox(width: 16),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        cart
            ? Positioned(
                top: 0.0,
                right: 0.0,
                child: GestureDetector(
                  onTap: () {
                    bloc.removeAllItemsFromCart(product.id);
                  },
                  child: Image.asset("assets/icons/close.png"),
                ),
              )
            : Positioned(
                top: 0.0,
                right: 0.0,
                child: GestureDetector(
                  onTap: () {
                    _settingModalBottomSheet(context);
                  },
                  child: Image.asset("assets/icons/hint.png"),
                ),
              ),
        Positioned(
          top: 0.0,
          left: 0.0,
          child: cart
              ? GestureDetector(
                  onTap: () {
                    _settingModalBottomSheet(context);
                  },
                  child: Image.asset("assets/icons/hint_left.png"),
                )
              : Container(),
        ),
      ],
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Stack(
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 32, bottom: 64, left: 16, right: 16),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Химчистка Рубашек",
                        style: TextStyle(color: UIData.text_blue, fontSize: 20.0, fontFamily: "MuseoSansBold"),
                      ),
                      SizedBox(height: 24),
                      Text(
                        "А вот так это делается просто берешь и делаешь и делать можно много а можно вообще не делать или делать и делать или не делать и не делать или делать но не сильно",
                        maxLines: 5,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: UIData.text_blue, fontSize: 14.0, fontFamily: "MuseoSansReg"),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 8,
                right: 8,
                child: IconButton(
                  icon: Image.asset("assets/icons/close.png"),
                  onPressed: () {
                    Navigator.of(context).maybePop();
                  },
                  //padding: EdgeInsets.all(0.0),
                ),
              ),
            ],
          );
        });
  }
}

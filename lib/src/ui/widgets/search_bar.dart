import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundromat/src/utils/UIData.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({
    @required this.controller,
    @required this.focusNode,
  });

  final TextEditingController controller;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {

    return DecoratedBox(
      decoration: BoxDecoration(
        //color: Colors.green,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          //horizontal: 4,
          vertical: 8,
        ),
        child: Row(
          children: [
            Expanded(
              child: CupertinoTextField(
                decoration: BoxDecoration(border: null, borderRadius: BorderRadius.circular(8), color: Colors.white),
                style: TextStyle(),
                placeholder: "поиск",
                prefix: const Icon(
                  CupertinoIcons.search,
                  color: UIData.textfield_gray,
                ),
                suffix: GestureDetector(
                  onTap: (){
                    controller.text = "";
                  },
                  child: Icon(
                    CupertinoIcons.clear_thick,
                    color: UIData.textfield_gray,
                  ),
                ),
                controller: controller,
                focusNode: focusNode,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

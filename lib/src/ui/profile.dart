import 'package:flutter/material.dart';
import 'package:laundromat/src/models/product.dart';
import 'package:laundromat/src/ui/widgets/category_item.dart';
import 'package:laundromat/src/ui/widgets/settings_switcher.dart';
import 'package:laundromat/src/utils/UIData.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("admin", style: TextStyle(fontSize: 17.0,color: UIData.text_blue,fontFamily: "MuseoSansBold")),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      backgroundColor: UIData.background_gray,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 24.0),
            SettingsSwitcher(str: "Уведомления",divider: true,),
          ],
        ),
      ),
    );
  }
}

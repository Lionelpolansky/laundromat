import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laundromat/src/blocs/sign_in/sign_in.dart';
import 'package:laundromat/src/main_tabs.dart';
import 'package:laundromat/src/utils/UIData.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Войти", style: TextStyle(fontSize: 17.0, color: UIData.text_blue, fontFamily: "MuseoSansBold")),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      backgroundColor: UIData.background_gray,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(height: 24.0),
              Container(
                color: Colors.white,
                height: 119,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Theme(
                      data: Theme.of(context).copyWith(primaryColor: Color.fromRGBO(240, 240, 241, 0.5)),
                      child: TextFormField(
                        style: TextStyle(fontSize: 20.0, color: UIData.text_blue),
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        decoration: InputDecoration(
                          //hintStyle: TextStyle(fontSize: 16.0, color: Colors.white),
                          filled: true,
                          isDense: true,
                          fillColor: Colors.white,
                          labelText: "Логин",
                          labelStyle:
                              TextStyle(fontSize: 16.0, fontFamily: "MuseoSansReg", color: UIData.textfield_gray),
                          //contentPadding: EdgeInsets.fromLTRB(24.0, 15.0, 24.0, 15.0),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                        ),
                      ),
                    ),
                    Divider(color: UIData.textfield_gray, thickness: 1.0, endIndent: 16.0, indent: 16.0, height: 1.0),
                    Theme(
                      data: Theme.of(context).copyWith(primaryColor: Color.fromRGBO(240, 240, 241, 0.5)),
                      child: TextFormField(
                        style: TextStyle(fontSize: 20.0, color: UIData.text_blue),
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        controller: passwordController,
                        decoration: InputDecoration(
                          //hintStyle: TextStyle(fontSize: 16.0, color: Colors.white),
                          filled: true,
                          isDense: true,
                          fillColor: Colors.white,
                          labelText: "Пароль",
                          labelStyle:
                              TextStyle(fontSize: 16.0, fontFamily: "MuseoSansReg", color: UIData.textfield_gray),
                          //contentPadding: EdgeInsets.fromLTRB(24.0, 15.0, 24.0, 15.0),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16.0),
              Container(
                width: double.infinity,
                //color: Colors.amber,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: CupertinoButton(
                    color: UIData.button_blue,
                    child: Text(
                      "Войти",
                      style: TextStyle(fontSize: 16.0, fontFamily: "MuseoSansBold"),
                    ),
                    onPressed: () {
                      //_validateAndSend();
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => MainTabs()), (Route<dynamic> route) => false);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _validateAndSend() {
    BlocProvider.of<SignInBloc>(context).add(
      SignInPressed(
        email: emailController.text,
        password: passwordController.text,
      ),
    );
  }
}

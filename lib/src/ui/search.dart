import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:laundromat/src/blocs/cart_bloc.dart';
import 'package:laundromat/src/models/product.dart';
import 'package:laundromat/src/ui/widgets/category_item.dart';
import 'package:laundromat/src/ui/widgets/product_item.dart';
import 'package:laundromat/src/ui/widgets/search_bar.dart';
import 'package:laundromat/src/utils/UIData.dart';
import 'package:provider/provider.dart';

class SearchTab extends StatefulWidget {
  @override
  _SearchTabState createState() => _SearchTabState();
}

class _SearchTabState extends State<SearchTab> {
  TextEditingController _controller;
  FocusNode _focusNode;
  String _substr = '';

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController()..addListener(_onTextChanged);
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _controller.dispose();
    super.dispose();
  }

  void _onTextChanged() {
    setState(() {
      _substr = _controller.text;
    });
  }

  void cancelPressed() {
    _controller.text = "";
    FocusScope.of(context).requestFocus(new FocusNode());
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  Widget _buildSearchBox() {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            child: SearchBar(
              controller: _controller,
              focusNode: _focusNode,
            ),
          ),
          _substr == ""
              ? Container()
              : CupertinoButton(
                  padding: EdgeInsets.only(left: 12, right: 8),
                  child: Text("Отменить",
                      style: TextStyle(
                          fontSize: 14.0,
                          color: UIData.button_blue,
                          fontFamily: "MuseoSansReg",
                          decoration: TextDecoration.underline)),
                  onPressed: () => {cancelPressed()},
                ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<CartStateBloc>(context);
    //bloc.setCategory(null);
    final results = bloc.search(_substr);
    final categories = bloc.getCategories();

    _showcontent() {
      if (_substr == null || _substr == "") {
        return Expanded(
          child: ListView.builder(
            itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
              child: CategoryItem(
                category: categories[index],
              ),
            ),
            itemCount: categories.length,
          ),
        );
      } else {
        return Expanded(
          child: ListView.builder(
            itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.only(top: 8, left: 16, right: 16),
              child: ProductItem(
                product: results[index],
              ),
            ),
            itemCount: results.length,
          ),
        );
      }
    }

    return Scaffold(
      backgroundColor: UIData.background_gray,
      body: SafeArea(
        child: Column(
          children: [
            _buildSearchBox(),
            _showcontent(),
          ],
        ),
      ),
    );
  }
}
